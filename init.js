$(function(){
	$(".key-map").controlKeyInput({
	  allowChar: /[0-9\+]/,
	  allow: function(input, char){
	    if(char == "+" && ($.caretPosition(input) !=0 || input.value.indexOf(char) != -1 ) )
	      return false ;
	    return true ;	
	  },
	  failed: function(input, char){
	  	console.log("char: " + char + " is not acceptable")
	  },
	  success: function(input, char){
	  	console.log("char: " + char + " is accepted")
	  }
	});
})
